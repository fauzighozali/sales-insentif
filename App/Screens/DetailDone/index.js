import React from 'react'
import { Container } from "./container";
import { Screen } from "../../Components/Screen";
import { waitInteraction } from "../../Lib/renderingHandler";

export const DetailDoneScreen = waitInteraction(props => {
  return (
    <Screen headerType='main' name={'Detail Done'}>
      <Container {...props} />
    </Screen>
  )
});
