import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { Avatar } from 'react-native-ui-lib'
import { TextTampan } from "../../../Components/Text";
import { device } from "../../../Lib/dimensions";
import { compose, withHandlers } from 'recompose'
import { withNavigation } from 'react-navigation'

const enhance = compose(
  withNavigation,
  withHandlers({
    navigate: ({ navigation }) => (to) => () => navigation.navigate({ key: to, routeName: to })
  })
);

export const DetailDone = enhance(props => {
  return (
    <View style={styles.container}>
      <TextTampan style={[styles.nameTitle, styles.black]}>Turbin Rusak</TextTampan>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Description</TextTampan>
      <TextTampan style={styles.black, styles.textStyle}>Tiba-tiba turbin mati dengan gejala muncul asap</TextTampan>
      <View style={{
        position: 'absolute',
        top: 10,
        right: 0 }}>
          <View style={{
            paddingHorizontal: 15,
            paddingVertical: 3,
            backgroundColor: '#f44141',
            borderBottomLeftRadius: 10 }}>
            <TextTampan style={{ fontSize: 12, fontWeight: 'bold', color: '#fff' }}>
              Date : 02-11-2018 07:45
            </TextTampan>
          </View>
      </View>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Location</TextTampan>
      <TextTampan style={styles.black, styles.textStyle}>Side 2A</TextTampan>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Remark</TextTampan>
      <TextTampan style={styles.black, styles.textStyle}>Perlu pergantian perangkat, elektriknya aus</TextTampan>
      <View style={styles.containerStyle}>
        <TouchableOpacity
          onPress={props.navigate('ListSop')}
          style={styles.navBar}>
            <Text style={styles.titleStyle}>
              SOP
            </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
});

const styles = StyleSheet.create({
  container: {
    height: '90%',
    justifyContent: 'center',
    padding: 10
  },
  nameTitle: {
    fontSize: 22,
    marginTop: 50,
    fontWeight: 'bold'
  },
  textStyle: {
    paddingLeft: 20
  },
  black: {
    color: '#000'
  },
  navBar: {
    flex: 1,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerStyle: {
    backgroundColor: "#F7941D",
    marginTop: 100,
    height: 50,
    borderRadius: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleStyle: {
    fontWeight: "700",
    fontSize: 15,
    color: '#fff'
  },
});
