import React from 'react'
import { Container } from "./container";
import { Screen } from "../../Components/Screen";
import { waitInteraction } from "../../Lib/renderingHandler";

export const DetailInProgressScreen = waitInteraction(props => {
  return (
    <Screen headerType='main' name={'Detail InProgress'}>
      <Container {...props} />
    </Screen>
  )
});
