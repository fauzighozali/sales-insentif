import React from 'react'
import { Container } from "./container";
import { Screen } from "../../Components/Screen";
import { waitInteraction } from "../../Lib/renderingHandler";

export const DetailRequestScreen = waitInteraction(props => {
  return (
    <Screen headerType='main' name={'Detail Request'}>
      <Container {...props} />
    </Screen>
  )
});
