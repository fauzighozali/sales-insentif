import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Avatar } from 'react-native-ui-lib'
import { TextTampan } from "../../../Components/Text";
import { device } from "../../../Lib/dimensions";
import { compose, withHandlers } from 'recompose'

const enhance = compose(
  withHandlers({
    navigate: ({ navigation }) => (to) => () => navigation.navigate({ key: to, routeName: to })
  })
);

export const DetailRequest = enhance(({ navigate, profile }) => {
  return (
    <View style={styles.container}>
      <TextTampan style={[styles.nameTitle, styles.black]}>Turbin Rusak</TextTampan>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Description</TextTampan>
      <TextTampan style={[styles.black, styles.textStyle]}>Tiba-tiba turbin mati dengan gejala muncul asap</TextTampan>
      <View style={{
        position: 'absolute',
        top: 0,
        right: 0 }}>
          <View style={{
            paddingHorizontal: 15,
            paddingVertical: 3,
            backgroundColor: '#f44141',
            borderBottomLeftRadius: 10 }}>
            <TextTampan style={{ fontSize: 12, fontWeight: 'bold', color: '#fff' }}>
              Date : 02-11-2018 07:45
            </TextTampan>
          </View>
      </View>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Location</TextTampan>
      <TextTampan style={[styles.black, styles.textStyle]}>Side 2A</TextTampan>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Remark</TextTampan>
      <TextTampan style={[styles.black, styles.textStyle]}>Perlu pergantian perangkat, elektriknya aus</TextTampan>
    </View>
  )
});

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    padding: 10
  },
  nameTitle: {
    fontSize: 22,
    marginTop: 50,
    fontWeight: 'bold'
  },
  textStyle: {
    paddingLeft: 20
  },
  black: {
    color: '#000'
  },
  avatarContainer: {
    width: '100%',
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  avatar: {
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#fff'
  },
  avatarName: {
    justifyContent: 'center',
    marginLeft: 10
  }
});
