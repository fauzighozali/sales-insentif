import * as React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, FlatList } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { TextTampan } from "../../Components/Text";
import Icon from 'react-native-vector-icons/MaterialIcons'
import Timeline from 'react-native-timeline-flatlist'
import { compose, withState, withProps, withHandlers } from 'recompose'
import { withAnimations } from "../../Lib/renderingHandler";
import { withNavigation } from 'react-navigation'

const data = [
  {
    id: '1',
    title: 'Matikan Listrik',
    message: 'Supaya tidak terjadi konsleting (Inprogress)'
  },
  {
    id: '2',
    title: 'Lepas Kabel',
    message: 'Tidak mengganggu kerja dan safety (Done)'
  },
  {
    id: '3',
    title: 'Lepas Turbin',
    message: 'Perbaikan turbin tidak terpasang (Inprogress)'
  },
  {
    id: '4',
    title: 'Ganti Kabel Power',
    message: 'Pasang Kabel Power Baru'
  }
];

const enhance = compose(
  withNavigation,
  withProps({ inputs: [] }),
  withState('username', 'setUsername', '179310'),
  withState('password', 'setPassword', '179310'),
  withHandlers({
    initInput: ({ inputs }) => (input, id) => inputs[id] = input,
    loginClick: (
      {
        username,
        password,
        dispatchLogin
      }
    ) => () => dispatchLogin({ username, password }),
    navigate: ({ navigation }) => (to) => () => navigation.navigate({key: to, routeName: to})
  })
);

export const getList = enhance(props => {
  return (
    <TouchableOpacity
      style={{
      flexDirection: 'row',
      borderBottomWidth: 0.5,
      borderBottomColor: '#767573',
      paddingHorizontal: 10,
      paddingBottom: 20,
      paddingTop: 40,
      backgroundColor: props.item.type === 'unread' ? '#ffeef5' : '#fff' }}>
        <View style={{
          position: 'absolute',
          top: 5,
          right: 5 }}>
            <View style={styles.containerButton}>
              <TouchableOpacity
                style={styles.navBar}
                loading={props.loading}
                onPress={props.navigate('ViewSop')}>
                  <Text style={styles.titleStyle}>
                    View
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.navBarTwo}
                loading={props.loading}
                onPress={props.navigate('UpdateSop')}>
                  <Text style={styles.titleStyle}>
                    Update
                  </Text>
              </TouchableOpacity>
            </View>
        </View>
        <View style={{
          flex: 0.2,
          justifyContent: 'center',
          alignItems: 'center' }}>
            {
              props.item.type === 'unread' &&
              <View style={{
                position: 'absolute',
                right: 18,
                bottom: 10,
                width: 10,
                height: 10,
                borderRadius: 10 / 2,
                backgroundColor: '#3ec1b8',
                zIndex: 1 }}/>
            }
          <View style={{
            height: 50,
            width: 50,
            borderRadius: 10,
            backgroundColor: '#fe5b65',
            justifyContent: 'center',
            alignItems: 'center' }}>
              <Icon name={props.item.type === 'read' ? 'drafts' : 'mail'} size={30} color={'#fff'}/>
          </View>
        </View>
        <View style={{ flex: 0.8, justifyContent: 'center' }}>
          <TextTampan style={{ color: '#000', fontWeight: 'bold' }}>
            {props.item.title}
          </TextTampan>
          <TextTampan>
            {props.item.message}
          </TextTampan>
        </View>
    </TouchableOpacity>
  )
});

const getData = withAnimations('zoomInDown')(getList);

class ListSopActivity extends React.Component {
  render(){
    return(
      <View style={styles.container}>
        <FlatList
          renderItem={getData}
          data={data}
          keyExtractor={(item) => item.id}
        />
      </View>
    )
  }
}

export const Container = enhance(props => {
  return (
    <ListSopActivity {...props}/>
  )
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  flexContainer: {
    flexDirection: 'row',
    marginLeft: 0
  },
  tab: {
    backgroundColor: 'white'
  },
  indicator: {
    backgroundColor: '#f5b969'
  },
  content: {
    padding: 10,
    color: '#f5b969',
    fontSize: 15,
    fontWeight: 'bold'
  },
  navBar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  navBar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#F7941D",
    marginTop: 5,
    width: 60,
    height: 30,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  navBarTwo: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#F7941D",
    marginTop: 5,
    width: 60,
    height: 30,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  containerButton: {
    flexDirection: 'row'
  },
  containerStyle: {
    backgroundColor: "#F7941D",
    marginTop: 10,
    width: 50,
    height: 30,
    borderRadius: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleStyle: {
    fontWeight: "700",
    fontSize: 10,
    color: '#fff'
  },
});
