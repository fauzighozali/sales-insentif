import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { UpdateSop } from "./module/UpdateSop"
import { TextTampan } from "../../Components/Text"
import { compose } from 'recompose'
import { device } from "../../Lib/dimensions"

const enhance = compose(

);

export const Container = enhance(props => {
  return (
    <View style={styles.container}>
      <View style={[styles.backgroundContainer, styles.absolute]}>
        <Image source={{ uri: 'https://ak0.picdn.net/shutterstock/videos/13905140/thumb/1.jpg' }}
               style={{
                 width: '100%',
                 height: device.height / 3.0,
                 resizeMode: 'cover'
               }}
        />
      </View>
      <View style={styles.containerText}>
          <TextTampan style={styles.textStyle}>Take photo</TextTampan>
      </View>
      <UpdateSop/>
    </View>
  )
});

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundContainer: {
    height: device.height / 2.5,
    borderBottomWidth: 90,
    borderBottomColor: 'transparent',
    zIndex: 0
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  containerText: {
    flex: 1,
    alignItems: 'center'
  },
  textStyle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 180,
    paddingLeft: 20,
    color: '#fff'
  },
});
