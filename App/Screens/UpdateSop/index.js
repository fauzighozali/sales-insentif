import React from 'react'
import { Container } from "./container";
import { Screen } from "../../Components/Screen";
import { waitInteraction } from "../../Lib/renderingHandler";

export const UpdateSopScreen = waitInteraction(props => {
  return (
    <Screen headerType='main' name={'Update Sop'}>
      <Container {...props} />
    </Screen>
  )
});
