import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { Avatar } from 'react-native-ui-lib'
import { TextTampan } from "../../../Components/Text";
import { CheckBox } from 'react-native-elements'
import { device } from "../../../Lib/dimensions";
import { compose, withHandlers } from 'recompose'

const enhance = compose(
  withHandlers({
    checked: false,
    navigate: ({ navigation }) => (to) => () => navigation.navigate({ key: to, routeName: to })
  })
);

export const UpdateSop = enhance(props => {

  return(
    <View style={styles.container}>
      <TextTampan style={[styles.nameTitle, styles.black]}>Turbin Rusak</TextTampan>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Status</TextTampan>
      <View containerCheckBox>
      <CheckBox
        title='In Progress'
        checked={props.checked}
        // onPress={() => this.setState({checked: !props.checked})}
        // onPress={{checked: !props.checked}}
        />
      <CheckBox
        title='Close'/>
      </View>
      <View style={{
        position: 'absolute',
        top: 0,
        right: 0 }}>
          <View style={{
            paddingHorizontal: 15,
            paddingVertical: 3,
            backgroundColor: '#f44141',
            borderBottomLeftRadius: 10 }}>
            <TextTampan style={{ fontSize: 12, fontWeight: 'bold', color: '#fff' }}>
              Date : 02-11-2018 07:45
            </TextTampan>
          </View>
      </View>
      <TextTampan style={{ color: '#000', marginTop: 10 }}>Remark</TextTampan>
      <TextTampan style={[styles.black, styles.textStyle]}>Perlu pergantian perangkat, elektriknya aus</TextTampan>
      <View style={styles.containerStyle}>
        <TouchableOpacity
          style={styles.navBar}>
            <Text style={styles.titleStyle}>
              Update
            </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
});

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    padding: 10
  },
  nameTitle: {
    fontSize: 22,
    marginTop: 230,
    fontWeight: 'bold'
  },
  textStyle: {
    paddingLeft: 20
  },
  black: {
    color: '#000'
  },
  containerCheckBox: {
    flexDirection: 'row'
  },
  navBar: {
    flex: 1,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerStyle: {
    backgroundColor: "#F7941D",
    marginTop: 50,
    height: 50,
    borderRadius: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleStyle: {
    fontWeight: "700",
    fontSize: 15,
    color: '#fff'
  },
});
