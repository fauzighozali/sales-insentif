import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { ViewSop } from "./module/ViewSop"
import { TextTampan } from "../../Components/Text"
import { compose } from 'recompose'
import { device } from "../../Lib/dimensions"

const enhance = compose(

);

export const Container = enhance(props => {
  return (
    <View style={styles.container}>
      <View style={[styles.backgroundContainer, styles.absolute]}>
        <Image source={{ uri: 'https://ak0.picdn.net/shutterstock/videos/13905140/thumb/1.jpg' }}
               style={{
                 width: '45%',
                 height: device.height / 4.0,
                 resizeMode: 'cover'
               }} />
      </View>
      <View style={[styles.backgroundContainerTwo, styles.absoluteTwo]}>
        <Image source={{ uri: 'https://ak0.picdn.net/shutterstock/videos/13905140/thumb/1.jpg' }}
               style={{
                 width: '100%',
                 height: device.height / 4.0,
                 resizeMode: 'cover'
               }} />
      </View>
      <View style={styles.containerText}>
        <TextTampan style={styles.textStyle}>Before</TextTampan>
        <TextTampan style={styles.textStyleTwo}>After</TextTampan>
      </View>
      <View style={styles.container}>
        <ViewSop/>
      </View>
    </View>
  )
});

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerText: {
    flexDirection: 'row'
  },
  backgroundContainer: {
    height: device.height / 2.5,
    borderBottomWidth: 90,
    borderBottomColor: 'transparent',
    zIndex: 0
  },
  backgroundContainerTwo: {
    borderBottomWidth: 90,
    borderBottomColor: 'transparent',
    zIndex: 0
  },
  absolute: {
    position: 'absolute',
    top: 10,
    left: 10,
    bottom: 0,
    right: 0
  },
  absoluteTwo: {
    position: 'absolute',
    top: 10,
    left: 215,
    bottom: 0,
    right: 10
  },
  textStyle: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#000',
    marginTop: 200,
    marginLeft: 80
  },
  textStyleTwo: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#000',
    marginTop: 200,
    marginLeft: 160
  }
});
