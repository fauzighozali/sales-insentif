import React from 'react'
import { Container } from "./container";
import { Screen } from "../../Components/Screen";
import { waitInteraction } from "../../Lib/renderingHandler";

export const ViewSopScreen = waitInteraction(props => {
  return (
    <Screen headerType='main' name={'View Sop'}>
      <Container {...props} />
    </Screen>
  )
});
